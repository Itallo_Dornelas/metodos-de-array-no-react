import { Component } from "react";
import Fruit from "./components/Fruit/Fruit";
import "./components/Fruit.css";

class App extends Component {
  render() {
    const fruits = [
      { name: "banana", color: "yellow", price: 2 },
      { name: "cherry", color: "red", price: 3 },
      { name: "strawberry", color: "red", price: 4 },
    ];
    const valueTotal = fruits.reduce((itemBefore, itemCurrent) => {
      return itemCurrent.price + itemBefore;
    }, 0);

    return (
      <div className="container">
        <div id="all-fruit-names">
          <h1>All Fruits</h1>
          {fruits.map((item) => {
            return <Fruit name={item.name} />;
          })}
        </div>
        <div id="red-fruit-names">
          <h1>Red Fruits</h1>
          {fruits
            .filter((item) => item.color === "red")
            .map((item) => {
              return <Fruit name={item.name} />;
            })}
        </div>
        <div id="total">
          <h1>Total Price</h1>
          <Fruit name={valueTotal} />
        </div>
      </div>
    );
  }
}

export default App;
