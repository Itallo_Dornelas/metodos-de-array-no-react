import { Component } from "react";

class Fruit extends Component {
  render() {
    return (
      <div>
        <ul>
          <li>{this.props.name}</li>
        </ul>
      </div>
    );
  }
}
export default Fruit;
